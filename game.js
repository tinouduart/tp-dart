let Player = require('./player');

const gameMode = {
    TOUR_DU_MONDE: "Le tour du monde",
    _301: "Le 301",
    CRICKET: "Le cricket"
}

module.exports = class game {
    constructor(mode, players){
        this.nbPlayers = 0;
        this.gameMode = null;
        this.playersList = [];
    }

    /* Renvoie le mode de jeu actuel */
    get mode(){
        return this.gameMode;
    }

    /* Renvoie la liste des joueurs */
    get players(){
        return this.playersList;
    }

    getPlayer(i){
        return this.playersList[i];
    }

    /* setters */
    setMode(mode){
        this.gameMode = mode;
    }

    setNbPlayers(_nbPlayers){
        this.nbPlayers = _nbPlayers;
    }

    setPlayers(players){
        this.playersList = players;
    }

    addPlayer(player){
        this.playersList.push(player);
    }

    isFinished(){
        switch (this.gameMode) {
            case gameMode.TOUR_DU_MONDE:
                for (let i = 0; i < this.nbPlayers; i++)
                {
                    if (this.playersList[i].score === 20)
                        return true;
                }
                return false;
                break;
            case gameMode._301:
                return true;
                break;
            case gameMode.CRICKET:
                return true;
                break;
            default:
                return true;
                break;
        }
    }

    calcPlayerScore(player, x){
        switch (this.gameMode) {
            case gameMode.TOUR_DU_MONDE:
                if (x > 0)
                {
                    score = player.score
                    player.setScore(score + 1);
                }
                break;
            case gameMode._301:
                break;
            case gameMode.CRICKET:
                break;

            default:
                break;
        }
    }
}