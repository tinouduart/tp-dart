var inquirer = require('inquirer');
let Player = require('./player');
let Game = require('./game');

/* Enumération des modes de jeu */
const gameMode = {
    TOUR_DU_MONDE: "Le tour du monde",
    _301: "Le 301",
    CRICKET: "Le cricket"
}

game = new Game();
player = new Player("toto");

var modeQuestion = [
    {
        type: 'list',
        name: 'mode',
        message: 'Sélectionnez le mode de jeu',
        choices: [gameMode.TOUR_DU_MONDE, gameMode._301, gameMode.CRICKET],
        filter: function(val) { /* On récupère le mode de jeu */
            game.setMode(val);
            return val.toLowerCase();
        }
    },
    {
        type: 'input',
        name: 'nbPlayers',
        message: 'Combien de joueurs ?',
        validate: function(value) { /* On récupère le nombre de joueurs */
            var valid = !isNaN(parseFloat(value));
            if (valid)
            {
                game.setNbPlayers(value); /* On set le nombre de joueurs */
                return valid
            }
            else
                return 'Veuillez entrer un nombre';
        },
        filter: Number
    }
]

async function init() {
    /* On pose les questions */
    const answers = await inquirer.prompt(modeQuestion)

    let playersQuestions = [];

    /* On récupère le nombre de joueurs */
    let nbPlayers = game.nbPlayers;

    questionsTourDuMonde = [];

    /* Boucle sur le nombre de joueurs pour créer les prochaines questions (ie: chaque joueur aura les questions) */
    for (let i = 0; i < nbPlayers; i++)
    {
        playersQuestions.push({
            type: 'input',
            name: `playerName${i}`,
            message: `Nom du joueur ${i+1} : `
        });
    }

    /* On boucle jusqu'à 3 (3 fléchettes pour le premier mode) */
    for (let i = 0; i < 3; i++) /* Création des questions */
    {
        questionsTourDuMonde.push({
            type: 'input',
            name: `scoreFlechette${i}`,
            message: `fléchette ${i+1} : `,
            validate: function(value) { /* On récupère le nombre de joueurs */
                var valid = !isNaN(parseFloat(value));
                    return valid || 'Veuillez entrer un nombre';
            },
            filter: Number
        })
    }

    /* On va demander les noms des joueurs */
    const answers2 = await inquirer.prompt(playersQuestions);

    /* On boucle sur le nombre de joueurs pour pouvoir tous les ajouter à l'objet game */
    for (let i = 0; i < nbPlayers; i++)
    {
        name = answers2[`playerName${i}`]

        player = new Player(name);
        game.addPlayer(player);
    }


    let currentPlayer = 0;
    while (!game.isFinished()) {
        console.log(`Joueur actuel : ${game.getPlayer(currentPlayer).name}`)
        
        const ans3 = await inquirer.prompt(questionsTourDuMonde);
        for (let i = 0; i < 3; i++)
        {
            score = ans3[`scoreFlechette${i}`];
            game.calcPlayerScore(game.getPlayer(currentPlayer), score);
            if (game.isFinished())
                break;
        }
        console.log(`${game.getPlayer(currentPlayer).name} score : ${game.getPlayer(currentPlayer).score}`)
        
        /* Dès qu'on a finit de poser les 3 questions, on incrémente l'index du joueur actuel */
        currentPlayer++;
        if (currentPlayer === nbPlayers) /* Si on arrive à nbPlayers, on repasse à 0 sinon on sort du tableau */
            currentPlayer = 0;
    }

    /* On cherche qui a gagné */
    for (let i = 0; i < nbPlayers; i++)
    {
        if (game.getPlayer(i).score === 20)
        {
            console.log(`Gagnant : ${game.getPlayer(i).name}`);
            break;
        }
    }
}
init();