module.exports = class player {
    constructor(name) {
        this.playerName = name;
        this.playerScore = 0;
    }

    /* getters */
    get name() {
        return this.playerName;
    }

    get score() {
        return this.playerScore;
    }

    /* setters */
    setName(name) {
        this.playerName = name;
    }

    setScore(score) {
        this.playerScore = score;
    }
}